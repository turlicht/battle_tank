TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    controller.cpp \
    proxy_hl.cpp \
    modules/ir_sensor.cpp \
    modules/monster_motor.cpp \
    modules/indicator.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    battle_tank.ino \
    config.h \
    controller.h \
    proxy_hl.h \
    modules/ir_sensor.h \
    modules/monster_motor.h \
    modules/indicator.h \
    common.h

