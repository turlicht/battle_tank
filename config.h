#ifndef config_h
#define config_h

#define DEBUG                       true

#define LED_INDICATOR               10
#define LED_LASER                   11

// Проксирование сигнала от аппаратура Heng Long TK-YL-101-3 до RX-18
#define PROXY_HL                    true
#define PROXY_HL_DEBUG              false
#define PROXY_HL_RECIEVER_IN        2
#define PROXY_HL_RECIEVER_OUT       3
#define PROXY_HL_DECREASE           1  // Уменьшение скорости: 0 без изменений, 1 для стокового HL

// Инфракрасный датчик 
#define IR_SENSOR                   false
#define IR_SENSOR_IN                11
#define IR_SENSOR_CREATIVE_RM_820   true

// Monster Motor Shield
#define MONSTER_MOTOR               true
#define MONSTER_MOTOR_DEBUG         true
#define MOTOR1_DIR1                 7
#define MOTOR1_DIR2                 8
#define MOTOR1_PWM                  5
#define MOTOR2_DIR1                 4
#define MOTOR2_DIR2                 9
#define MOTOR2_PWM                  6
//#define MOTOR1_SENSOR               2
//#define MOTOR2_SENSOR               3
#define MOTOR_BLOCKING_PERIOD       1000000

#define MOTOR_LF_MAP_FROM              30
#define MOTOR_LF_MAP_TO                100
#define MOTOR_LB_MAP_FROM              30
#define MOTOR_LB_MAP_TO                100

#define MOTOR_RF_MAP_FROM              30
#define MOTOR_RF_MAP_TO                100
#define MOTOR_RB_MAP_FROM              0
#define MOTOR_RB_MAP_TO                255


#endif
