#include "config.h"

#if PROXY_HL
  #include "proxy_hl.h"
  ProxyHL proxy_hl(PROXY_HL_RECIEVER_IN, PROXY_HL_RECIEVER_OUT);
#endif

#if IR_SENSOR
  #include "ir_sensor.h"
  IRSensor ir_sensor(IR_SENSOR_IN);
#endif

#include "controller.h"
Controller controller;

void setup() {
  #if DEBUG
    Serial.begin(9600);
    Serial.println("Starting");
  #endif
}

void loop() {
  unsigned long mks = micros();

  #if PROXY_HL  
    proxy_hl.loop();
  #endif

  #if IR_SENSOR  
    ir_sensor.loop(mks);
  #endif

  controller.loop(mks);
}










