#ifndef COMMON
#define COMMON

enum class TankState {
    None,
    PreWelcoming,   // Приветствующее моргание светодиодом
    Welcoming,      // Приветствующее моргание светодиодом
    PreWaiting,     // Ожидание включения зажигания
    Waiting,        // Ожидание включения зажигания
    Ignition,       // Заведение танка
    Acting,         // Активные действия танка
    Damaged,        // Повреждение танка
    Destroyed       // Уничтожение танка
};

class TankEvents {
public:
    virtual void finish_blink() = 0;
    virtual void move(int move_value, int turn_value) = 0;
    virtual void stop() = 0;
};

#endif // COMMON
