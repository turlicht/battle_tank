/*
 * Controller
 */

#ifndef controller_h
#define controller_h

#include "config.h"
#include "common.h"
#include "modules/indicator.h"

#if MONSTER_MOTOR
  #include "modules/monster_motor.h"
#endif


class Controller : public TankEvents
{
  public:
    Controller();
    void loop(unsigned long mks);

    virtual void finish_blink() override;
    virtual void move(int move_value, int turn_value) override;
    virtual void stop() override;

    TankState state = TankState::PreWelcoming;
    
  private:
    Indicator indicator;
    #if MONSTER_MOTOR
      MonsterMotor monsterMotor;
    #endif

    unsigned long curr_mks = 0;
};

#endif
