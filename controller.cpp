#include "controller.h"
#include "Arduino.h"


Controller::Controller()
    : indicator(*this, LED_INDICATOR)
{
  pinMode(LED_LASER, OUTPUT);
  digitalWrite(LED_LASER, LOW);

  pinMode(LED_INDICATOR, OUTPUT);
  digitalWrite(LED_INDICATOR, HIGH);
}

void Controller::loop(unsigned long mks)
{
    curr_mks = mks;

    indicator.loop(mks);

    switch (state) {
    case TankState::PreWelcoming:
        state = TankState::Welcoming;
        indicator.blink(3, 400000, 400000, 0);
        break;
    case TankState::Welcoming:
        break;
    case TankState::PreWaiting:
        state = TankState::Waiting;
        indicator.blink(999999, 1000000, 1000000, 0);
    case TankState::Waiting:
        break;
    }
  
    #if MONSTER_MOTOR
        monsterMotor.loop(mks);
    #endif
}

void Controller::finish_blink()
{
    switch (state) {
    case TankState::Welcoming:
        state = TankState::PreWaiting;
        break;
    }
}

void Controller::move(int move_value, int turn_value)
{
  #if MONSTER_MOTOR
    monsterMotor.movement(move_value, turn_value);
  #endif
}

void Controller::stop()
{
  #if MONSTER_MOTOR
    monsterMotor.stopping();
  #endif
}

