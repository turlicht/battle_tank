/*
 * Monster Motor Shield
 */

#ifndef monster_motor_h
#define monster_motor_h

#include "../config.h"


class MonsterMotor
{
  public:
    MonsterMotor();
    void loop(unsigned long mks);

    void movement(int move_value, int turn_value);
    void stopping();

  private:
    bool is_stop = false;
    unsigned long curr_time = 0;
    unsigned long last_command_time = 0;

};

#endif
