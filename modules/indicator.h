#ifndef INDICATOR_H
#define INDICATOR_H


#include "../common.h"


class Indicator
{
public:
    Indicator(TankEvents &events, int pin_out);
    int pin_out;

    void loop(unsigned long mks);

    void blink(int n, unsigned long t1, unsigned long t2, unsigned long t3);

private:
    TankEvents &events;
    unsigned long curr_mks = 0;

    bool is_indicate_light = false;                 // Признак, что в данный момент светодиод горит
    int indicate_count = 0;                         // Количество морганий
    unsigned long indicate_light_interval = 0;      // Время, которое будет гореть светодиод
    unsigned long indicate_no_light_interval = 0;   // Время, которое светодиод не будет гореть
    unsigned long indicate_prev_mks = 0;            // Время последней смены состояния

};

#endif // INDICATOR_H
