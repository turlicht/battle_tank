#include "indicator.h"
#include "Arduino.h"


Indicator::Indicator(TankEvents &events, int pin_out)
  : events(events), pin_out(pin_out)
{
    pinMode(pin_out, OUTPUT);
    digitalWrite(pin_out, HIGH);
}

void Indicator::loop(unsigned long mks)
{
    curr_mks = mks;

    if (indicate_count) {
        if (is_indicate_light) {
            if (mks - indicate_prev_mks > indicate_light_interval) {
                indicate_prev_mks = mks;
                is_indicate_light = false;
                digitalWrite(pin_out, LOW);
            }
        }
        else {
            if (mks - indicate_prev_mks > indicate_no_light_interval) {
                indicate_prev_mks = mks;
                is_indicate_light = true;
                indicate_count -= 1;
                if (indicate_count == 0){
                    digitalWrite(pin_out, LOW);
                    events.finish_blink();
                }
                else {
                    digitalWrite(pin_out, HIGH);
                }
            }
        }
    }
}

void Indicator::blink(int n, unsigned long t1, unsigned long t2, unsigned long t3)
{
    is_indicate_light = true;
    indicate_count = n;
    indicate_light_interval = t1;
    indicate_no_light_interval = t2;
    indicate_prev_mks = curr_mks;
    digitalWrite(pin_out, HIGH);
}

