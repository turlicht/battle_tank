#include "monster_motor.h"
#include "Arduino.h"


MonsterMotor::MonsterMotor()
{
  pinMode(MOTOR1_DIR1, OUTPUT);
  pinMode(MOTOR1_DIR2, OUTPUT);
  pinMode(MOTOR1_PWM, OUTPUT);
  pinMode(MOTOR2_DIR1, OUTPUT);
  pinMode(MOTOR2_DIR2, OUTPUT);
  pinMode(MOTOR2_PWM, OUTPUT);

  stopping();
}

void MonsterMotor::loop(unsigned long mks)
{
  curr_time = mks;
  if (last_command_time + MOTOR_BLOCKING_PERIOD < curr_time) {
    stopping();
  }
}

void MonsterMotor::movement(int move_value, int turn_value)
{
  is_stop = false;
  last_command_time = curr_time;

  int m_left = 0;
  int m_right = 0;
  
  if (move_value >= 0) {
    if (turn_value >= 0) {
      if (move_value >= turn_value) {
        m_right = move_value;
        m_left = move_value - turn_value;
      }
      else {
        m_right = turn_value;
        m_left = -turn_value + move_value;
      }
    }
    else if (turn_value < 0) {
      if (move_value >= -turn_value) {
        m_left = move_value;
        m_right = move_value + turn_value;
      }
      else {
        m_left = -turn_value;
        m_right = turn_value + move_value;
      }      
    }
//    else {
//        m_left = move_value;
//        m_right = move_value;      
//    }
  }
  else if (move_value < 0) {
    m_left = move_value;
    m_right = move_value;    
  }


  if (m_left >= 0) {
    m_left = map(m_left, 0, 255, MOTOR_LF_MAP_FROM, MOTOR_LF_MAP_TO);
    analogWrite(MOTOR1_PWM, m_left);    
    digitalWrite(MOTOR1_DIR1, HIGH);
    digitalWrite(MOTOR1_DIR2, LOW);
  }
  else {
    m_left = map(-m_left, 0, 255, MOTOR_LB_MAP_FROM, MOTOR_LB_MAP_TO);
    analogWrite(MOTOR1_PWM, m_left);
    digitalWrite(MOTOR1_DIR1, LOW);
    digitalWrite(MOTOR1_DIR2, HIGH);
  }

  if (m_right >= 0) {
    m_right = map(m_right, 0, 255, MOTOR_RF_MAP_FROM, MOTOR_RF_MAP_TO);
    analogWrite(MOTOR2_PWM, m_right);
    digitalWrite(MOTOR2_DIR1, HIGH);
    digitalWrite(MOTOR2_DIR2, LOW);
  }
  else {
    m_right = map(-m_right, 0, 255, MOTOR_RB_MAP_FROM, MOTOR_RB_MAP_TO);
    analogWrite(MOTOR2_PWM, m_right);
    digitalWrite(MOTOR2_DIR1, LOW);
    digitalWrite(MOTOR2_DIR2, HIGH);
  }

  #if DEBUG && MONSTER_MOTOR_DEBUG
    Serial.print(move_value);
    Serial.print(" ");
    Serial.print(turn_value);  
    Serial.print(" ");
    Serial.print(m_left);
    Serial.print(" ");
    Serial.println(m_right);
  #endif      
}

void MonsterMotor::stopping()
{
  if (!is_stop) {
    digitalWrite(MOTOR1_DIR1, LOW);
    digitalWrite(MOTOR1_DIR2, LOW);
    digitalWrite(MOTOR1_PWM, 0);  
    digitalWrite(MOTOR2_DIR1, LOW);
    digitalWrite(MOTOR2_DIR2, LOW);
    digitalWrite(MOTOR2_PWM, 0);
    is_stop = true;
  }
}

