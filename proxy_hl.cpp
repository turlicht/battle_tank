#include "proxy_hl.h"
#include "Arduino.h"


ProxyHL::ProxyHL(int pin_in, int pin_out)
  : pin_in(pin_in),
    pin_out(pin_out)
{
  pinMode(pin_in, INPUT);
  pinMode(pin_out, OUTPUT);
  
  package_send = RC_IDLE;
}


void ProxyHL::loop()
{
    mks_curr = micros();
    rc_state_curr = digitalRead(pin_in);

  // Проверяем перепады уровней
  if (rc_state_curr && !rc_state_prev) {
    if (rc_package_started) {
      process_signal(0);
    }
    else {
      rc_pre_package_started = true;
      rc_pre_package_start_mks = mks_curr;
      digitalWrite(pin_out, rc_state_curr);
    }
  }
  else if (!rc_state_curr && rc_state_prev) {
    if (rc_package_started) {
      process_signal(1);
    }
    else {
      if (rc_pre_package_started) {   
        unsigned long pre_package_duration = mks_curr - rc_pre_package_start_mks;
        if (pre_package_duration < 1150 && pre_package_duration > 1000) {
          rc_package_started = true;
          rc_prev_bit_mks = mks_curr;

          // Первый бит - единица
          package_bits = 1;
          package = 1;

          package_move = 0;
          package_turn = 0;
          
          is_pre_state_redifined = false;
          rc_prev_bit_mks = mks_curr;
        }
      }
      
      rc_pre_package_started = false;
      digitalWrite(pin_out, rc_state_curr);      
    }
  }

  if (rc_package_started && !is_pre_state_redifined) {
    unsigned long bit_mks = mks_curr - rc_prev_bit_mks;
    if (bit_mks > 300) {
      
      int s = bitRead(package_send, 31 - package_bits);
      digitalWrite(pin_out, s);
      is_pre_state_redifined = true;    
    }
  }
  
  rc_state_prev = rc_state_curr;
  mks_prev = mks_curr;
}


void ProxyHL::process_signal(unsigned char s)
{
  unsigned long bit_mks = mks_curr - rc_prev_bit_mks;
  if (bit_mks > 500 && bit_mks < 700) {
    rc_prev_bit_mks = mks_curr;
    is_pre_state_redifined = false;
    
    package_bits++;
    package = package << 1;
    package = package | s;

    switch (package_bits) {
      case 11:
        package_move = package_move | s;
        package_move = package_move << 1;
        break;
      case 12:
        package_move = package_move | s;
        package_move = package_move << 1;
        break;
      case 13:
        package_move = package_move | s;
        package_move = package_move << 1;
        break;
      case 14:
        package_move = package_move | s; 
        break;

      case 21:
        package_turn = package_turn | s;
        package_turn = package_turn << 1;
        break;
      case 22:
        package_turn = package_turn | s;
        package_turn = package_turn << 1;
        break;
      case 23:
        package_turn = package_turn | s;
        package_turn = package_turn << 1;
        break;
      case 24:
        package_turn = package_turn | s;
        break;
      
      case 32:
        process_package();
        rc_package_started = false;
        break;
    }

    digitalWrite(pin_out, !bitRead(package_send, 32 - package_bits));
  }
  else if (bit_mks > 100 && bit_mks <= 400) {
  }
  else {
    rc_package_started = false;
    digitalWrite(pin_out, rc_state_curr);
 }
}

void ProxyHL::process_package()
{
    package_send = package;

    // Стреляем кнопкой K
    if (package & RC_CANNON) {
      bitWrite(package_send, 17, false);
      bitWrite(package_send, 13, true);  
    }
    else {
      bitWrite(package_send, 13, false);
    }
    
    if (package & RC_TURRET_FIRE) {
      bitWrite(package_send, 14, true);
    }

    bool is_move = false;
    int m_move = 0;
    int m_turn = 0;
    
    if (package_send & RC_REVERSE) {
      is_move = true;
      m_move = -package_move;

      #if PROXY_HL_DECREASE == 1
        switch (package_move) {
          case 0:  set_reverse_speed_02(package_send); break;
          case 1:  set_reverse_speed_02(package_send); break;
          case 2:  set_reverse_speed_02(package_send); break;
          case 3:  set_reverse_speed_03(package_send); break;
          case 4:  set_reverse_speed_03(package_send); break;
          case 5:  set_reverse_speed_04(package_send); break;
          case 6:  set_reverse_speed_04(package_send); break;
          case 7:  set_reverse_speed_04(package_send); break;
          case 8:  set_reverse_speed_05(package_send); break;
          case 9:  set_reverse_speed_05(package_send); break;
          case 10: set_reverse_speed_06(package_send); break;
          case 11: set_reverse_speed_06(package_send); break;
          case 12: set_reverse_speed_06(package_send); break;
          case 13: set_reverse_speed_07(package_send); break;
          case 14: set_reverse_speed_07(package_send); break;     
        }
      #endif
    }
    else if (package_move != 15) {  
      is_move = true;
      m_move = 15 - package_move;
      if (m_move == 15) {
        m_move = 14;
      }

      #if PROXY_HL_DECREASE == 1
        switch (package_move) {
          case 14: set_forward_speed_02(package_send); break;
          case 13: set_forward_speed_02(package_send); break;
          case 12: set_forward_speed_02(package_send); break;
          case 11: set_forward_speed_03(package_send); break;
          case 10: set_forward_speed_03(package_send); break;
          case 9:  set_forward_speed_04(package_send); break;
          case 8:  set_forward_speed_04(package_send); break;
          case 7:  set_forward_speed_04(package_send); break;
          case 6:  set_forward_speed_05(package_send); break;
          case 5:  set_forward_speed_05(package_send); break;
          case 4:  set_forward_speed_06(package_send); break;
          case 3:  set_forward_speed_06(package_send); break;
          case 2:  set_forward_speed_07(package_send); break;
          case 1:  set_forward_speed_07(package_send); break;
          case 0:  set_forward_speed_07(package_send); break;     
        }
      #endif        
    }

    if (package_send & RC_RIGHT) {
      is_move = true;
      m_turn = -package_turn;

      #if PROXY_HL_DECREASE == 1
        switch (package_turn) {
          case 0:  set_right_speed_01(package_send); break;
          case 1:  set_right_speed_01(package_send); break;
          case 2:  set_right_speed_02(package_send); break;
          case 3:  set_right_speed_03(package_send); break;
          case 4:  set_right_speed_03(package_send); break;
          case 5:  set_right_speed_04(package_send); break;
          case 6:  set_right_speed_05(package_send); break;
          case 7:  set_right_speed_05(package_send); break;
          case 8:  set_right_speed_06(package_send); break;
          case 9:  set_right_speed_07(package_send); break;
          case 10: set_right_speed_07(package_send); break;
          case 11: set_right_speed_08(package_send); break;
          case 12: set_right_speed_09(package_send); break;
          case 13: set_right_speed_09(package_send); break;
          case 14: set_right_speed_10(package_send); break;     
        }
      #endif        
    }
    else if (package_turn != 15) {  
      is_move = true;
      m_turn = 15 - package_turn;
      if (m_turn == 15) {
        m_turn = 14;
      }

      #if PROXY_HL_DECREASE == 1
        switch (package_turn) {
          case 14: set_left_speed_01(package_send); break;
          case 13: set_left_speed_01(package_send); break;
          case 12: set_left_speed_02(package_send); break;
          case 11: set_left_speed_03(package_send); break;
          case 10: set_left_speed_03(package_send); break;
          case 9:  set_left_speed_04(package_send); break;
          case 8:  set_left_speed_05(package_send); break;
          case 7:  set_left_speed_05(package_send); break;
          case 6:  set_left_speed_06(package_send); break;
          case 5:  set_left_speed_07(package_send); break;
          case 4:  set_left_speed_07(package_send); break;
          case 3:  set_left_speed_08(package_send); break;
          case 2:  set_left_speed_09(package_send); break;
          case 1:  set_left_speed_09(package_send); break;
          case 0:  set_left_speed_10(package_send); break;     
        }
      #endif   
    }

    if (is_move) {
      controller.move(map(m_move, 0, 14, 0, 255), map(m_turn, 0, 14, 0, 255));
    }
    else {
      controller.stop();
    }

    set_crc();

    #if DEBUG && PROXY_HL_DEBUG
      print_package(package_send);
    #endif    
}

void ProxyHL::set_crc()
{
  bool t = 0;

  t = bitRead(package_send, 23) ^ bitRead(package_send, 19) ^ bitRead(package_send, 15) ^ bitRead(package_send, 11);
  bitWrite(package_send, 5, t);
  t = bitRead(package_send, 22) ^ bitRead(package_send, 18) ^ bitRead(package_send, 14) ^ bitRead(package_send, 10);
  bitWrite(package_send, 4, t);
  t = bitRead(package_send, 21) ^ bitRead(package_send, 17) ^ bitRead(package_send, 13) ^ bitRead(package_send, 9) ^ bitRead(package_send, 7);
  bitWrite(package_send, 3, t);
  t = bitRead(package_send, 20) ^ bitRead(package_send, 16) ^ bitRead(package_send, 12) ^ bitRead(package_send, 8) ^ bitRead(package_send, 6);
  bitWrite(package_send, 2, t);
}

void ProxyHL::print_package(unsigned long p)
{ 
  Serial.println(package, BIN);

//        if (rc_package == RC_IDLE) {
//          Serial.print(" IDLE");
//        }
//      
//        if (rc_package & 0b00000000010000000000000000000000) {
//          Serial.print(" REVERSE");
//          digitalWrite(LED_REVERSE, HIGH);
//          digitalWrite(LED_FORWARD, LOW);          
//        }
//        else if ((rc_package & 0b00000000001111000000000000000000) ^ 0b00000000001111000000000000000000) {
//          Serial.print(" FORWARD");
//          digitalWrite(LED_FORWARD, HIGH);
//          digitalWrite(LED_REVERSE, LOW);
//        }
//        else {
//          digitalWrite(LED_FORWARD, LOW);
//          digitalWrite(LED_REVERSE, LOW);
//        }
//
//        if (rc_package & 0b00000000000000000001000000000000) {
//          Serial.print(" RIGHT");
//        }
//        else if ((rc_package & 0b00000000000000000000111100000000) ^ 0b00000000000000000000111100000000) {
//          Serial.print(" LEFT");
//        }
//
//        if (rc_package & 0b00000000000000010000000000000000) {
//          Serial.print(" TURRET_RIGHT");
//        }
//
//        if (rc_package & 0b00000000000000001000000000000000) {
//          Serial.print(" TURRET_LEFT");
//        }
//
//        if (rc_package & 0b00000000000000000100000000000000) {
//          Serial.print(" TURRET_UP_DOWN");
//        }
//
//        if (rc_package & 0b00000000000000000010000000000000) {
//          Serial.print(" TURRET_FIRE");
//        }
//
//        if (rc_package & 0b00000000000000100000000000000000) {
//          Serial.print(" CANNON");
//        }
//
//        if (rc_package & 0b00000000000000000000000001000000) {
//          Serial.print(" MACHINE_GUN");
//        }
//
//        if (rc_package & 0b00000000000000000000000010000000) {
//          Serial.print(" IGNITION");
//        }  

//    Serial.println();
}

void ProxyHL::set_forward_speed_01(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 18, false);
}

void ProxyHL::set_forward_speed_02(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, true); bitWrite(p, 19, false); bitWrite(p, 18, true);
}

void ProxyHL::set_forward_speed_03(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, true); bitWrite(p, 19, false); bitWrite(p, 18, false);
}

void ProxyHL::set_forward_speed_04(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, true); bitWrite(p, 18, true);
}

void ProxyHL::set_forward_speed_05(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, true); bitWrite(p, 18, false);
}

void ProxyHL::set_forward_speed_06(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, true);
}

void ProxyHL::set_forward_speed_07(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, false);
}

void ProxyHL::set_forward_speed_08(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 18, true);
}

void ProxyHL::set_forward_speed_09(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 18, false);
}

void ProxyHL::set_forward_speed_10(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, false); bitWrite(p, 18, true);
}



void ProxyHL::set_reverse_speed_01(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, false);
}

void ProxyHL::set_reverse_speed_02(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, true);
}

void ProxyHL::set_reverse_speed_03(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, false); bitWrite(p, 19, true); bitWrite(p, 18, false);
}

void ProxyHL::set_reverse_speed_04(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, false); bitWrite(p, 19, true); bitWrite(p, 18, true);
}

void ProxyHL::set_reverse_speed_05(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, false); bitWrite(p, 18, false);
}

void ProxyHL::set_reverse_speed_06(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, false); bitWrite(p, 18, true);
}

void ProxyHL::set_reverse_speed_07(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 18, false);
}

void ProxyHL::set_reverse_speed_08(unsigned long &p) {
  bitWrite(p, 21, false); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 18, true);
}

void ProxyHL::set_reverse_speed_09(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, false);
}

void ProxyHL::set_reverse_speed_10(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, false); bitWrite(p, 19, false); bitWrite(p, 18, true);
}



void ProxyHL::set_left_speed_01(unsigned long &p) {
  bitWrite(p, 21, true); bitWrite(p, 20, true); bitWrite(p, 19, true); bitWrite(p, 8, false);
}

void ProxyHL::set_left_speed_02(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, true); bitWrite(p, 9, false); bitWrite(p, 8, true);
}

void ProxyHL::set_left_speed_03(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, true); bitWrite(p, 9, false); bitWrite(p, 8, false);
}

void ProxyHL::set_left_speed_04(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, true); bitWrite(p, 8, true);
}

void ProxyHL::set_left_speed_05(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, true); bitWrite(p, 8, false);
}

void ProxyHL::set_left_speed_06(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, true);
}

void ProxyHL::set_left_speed_07(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, false);
}

void ProxyHL::set_left_speed_08(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, true); bitWrite(p, 8, true);
}

void ProxyHL::set_left_speed_09(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, true); bitWrite(p, 8, false);
}

void ProxyHL::set_left_speed_10(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, false); bitWrite(p, 8, true);
}




void ProxyHL::set_right_speed_01(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, false);
}

void ProxyHL::set_right_speed_02(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, true);
}

void ProxyHL::set_right_speed_03(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, false); bitWrite(p, 9, true); bitWrite(p, 8, false);
}

void ProxyHL::set_right_speed_04(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, false); bitWrite(p, 9, true); bitWrite(p, 8, true);
}

void ProxyHL::set_right_speed_05(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, false); bitWrite(p, 8, false);
}

void ProxyHL::set_right_speed_06(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, false); bitWrite(p, 8, true);
}

void ProxyHL::set_right_speed_07(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, true); bitWrite(p, 8, false);
}

void ProxyHL::set_right_speed_08(unsigned long &p) {
  bitWrite(p, 11, false); bitWrite(p, 10, true); bitWrite(p, 9, true); bitWrite(p, 8, true);
}

void ProxyHL::set_right_speed_09(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, false);
}

void ProxyHL::set_right_speed_10(unsigned long &p) {
  bitWrite(p, 11, true); bitWrite(p, 10, false); bitWrite(p, 9, false); bitWrite(p, 8, true);
}

