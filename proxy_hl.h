/*
 * Proxy Heng Long TK-YL-101-3
 */

#ifndef proxy_hl_h
#define proxy_hl_h

#include "config.h"
#include "controller.h"

#define RC_IDLE           0b11111110001111000000111100000000
#define RC_FORWARD        0b00000000001111000000000000000000
#define RC_REVERSE        0b00000000010000000000000000000000
#define RC_LEFT           0b00000000000000000000111100000000
#define RC_RIGHT          0b00000000000000000001000000000000

#define RC_IGNITION       0b00000000000000000000000010000000
#define RC_MACHINE_GUN    0b00000000000000000000000001000000
#define RC_CANNON         0b00000000000000100000000000000000
#define RC_TURRET_RIGHT   0b00000000000000010000000000000000
#define RC_TURRET_LEFT    0b00000000000000001000000000000000
#define RC_TURRET_UP_DOWN 0b00000000000000000100000000000000
#define RC_TURRET_FIRE    0b00000000000000000010000000000000

class ProxyHL
{
  public:
    ProxyHL(int pin_in, int pin_out);
    void loop();

    int pin_in;
    int pin_out;

  private:
    Controller controller;
  
    void print_package(unsigned long p);
    void process_signal(unsigned char s);
    void process_package();
    void set_crc();
  
    unsigned long mks_prev = 0;   // Предыдущее состояние времени
    unsigned long mks_curr = 0;   // Текущее состояние времени
    
    bool rc_state_prev = false;   // Предыдущее состояние сигнала
    bool rc_state_curr = false;   // Текущее состояние сигнала
        
    bool rc_package_started = false;            // Начали ли примимать биты пакета
    bool rc_pre_package_started = false;        // Начался ли предварительный импульс перед пакетом
    unsigned long rc_pre_package_start_mks = 0; // Время предварительного импульса
    unsigned long rc_prev_bit_mks = 0;          // Время предыдущего сигнала
    
    int package_bits;                      // Сколько бит обработано в пакете    
    unsigned long package;                 // Получаемый пакет
    unsigned long package_send = 0;        // Пакет, который отправим
    int package_crc;                       // Контрольная сумма
    bool is_pre_state_redifined = false;   // Признак, что выставлен уровень между сигналами

    unsigned char package_move = 0;
    unsigned char package_turn = 0;

    void set_forward_speed_01(unsigned long &p);
    void set_forward_speed_02(unsigned long &p);
    void set_forward_speed_03(unsigned long &p);
    void set_forward_speed_04(unsigned long &p);
    void set_forward_speed_05(unsigned long &p);
    void set_forward_speed_06(unsigned long &p);
    void set_forward_speed_07(unsigned long &p);
    void set_forward_speed_08(unsigned long &p);
    void set_forward_speed_09(unsigned long &p);
    void set_forward_speed_10(unsigned long &p);
    void set_reverse_speed_01(unsigned long &p);
    void set_reverse_speed_02(unsigned long &p);
    void set_reverse_speed_03(unsigned long &p);
    void set_reverse_speed_04(unsigned long &p);
    void set_reverse_speed_05(unsigned long &p);
    void set_reverse_speed_06(unsigned long &p);
    void set_reverse_speed_07(unsigned long &p);
    void set_reverse_speed_08(unsigned long &p);
    void set_reverse_speed_09(unsigned long &p);
    void set_reverse_speed_10(unsigned long &p);
    void set_left_speed_01(unsigned long &p);
    void set_left_speed_02(unsigned long &p);
    void set_left_speed_03(unsigned long &p);
    void set_left_speed_04(unsigned long &p);
    void set_left_speed_05(unsigned long &p);
    void set_left_speed_06(unsigned long &p);
    void set_left_speed_07(unsigned long &p);
    void set_left_speed_08(unsigned long &p);
    void set_left_speed_09(unsigned long &p);
    void set_left_speed_10(unsigned long &p);
    void set_right_speed_01(unsigned long &p);
    void set_right_speed_02(unsigned long &p);
    void set_right_speed_03(unsigned long &p);
    void set_right_speed_04(unsigned long &p);
    void set_right_speed_05(unsigned long &p);
    void set_right_speed_06(unsigned long &p);
    void set_right_speed_07(unsigned long &p);
    void set_right_speed_08(unsigned long &p);
    void set_right_speed_09(unsigned long &p);
    void set_right_speed_10(unsigned long &p);    
};


#endif
